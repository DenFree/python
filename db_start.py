import sqlite3




conn = sqlite3.connect("mydatabase.db") # или :memory: чтобы сохранить в RAM
cursor = conn.cursor()

cursor.execute("""CREATE TABLE questions (
    id   INTEGER PRIMARY KEY AUTOINCREMENT,
    type INT,
    text TEXT
)""")
conn.commit()


cursor.execute("""CREATE TABLE options (
    id   INTEGER PRIMARY KEY AUTOINCREMENT,
    id_q         REFERENCES questions (id),
    text TEXT
)""")
conn.commit()


cursor.execute("""CREATE TABLE answers (
    id     INTEGER PRIMARY KEY AUTOINCREMENT,
    user   INT,
    id_q           REFERENCES questions (id),
    answer TEXT
)""")
conn.commit()


conn.close()
