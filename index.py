from flask import Flask, redirect, request, render_template, url_for, session
import sqlite3


app = Flask(__name__)




@app.route('/login')
def index():
    return 'Index Page'

@app.route('/hello')
def hello():
    return 'Hello World'


@app.route('/done')
def done():

    if 'page' in session:
        print('session ok')
    else:
        return redirect('/', code=302)

    user_id = 1
    user_name = session['name']

    conn = sqlite3.connect("mydatabase.db")

    print('user_name');
    print(user_name);

    cur = conn.cursor()
    cur.execute("SELECT * FROM answers WHERE user = '%s'" % user_name)

    options = cur.fetchall()

    return render_template('done.html', user_id=user_id, user_name=user_name, options=options)


@app.route('/game')
def show_user_profile():



    if 'page' in session:
        print('session ok')
    else:
        return redirect('/', code=302)


    id_q = session['page']

    conn = sqlite3.connect("mydatabase.db")

    print('id_q')
    print(id_q)

    cur = conn.cursor()
    sql = "SELECT * FROM questions WHERE id = ?"
    cur.execute(sql, str(id_q))
    sql_return = cur.fetchone();


    print('sql_return');
    print(sql_return);

    if sql_return is None:
        return redirect('/done', code=302)
    else:



        text = sql_return[2]
        type = sql_return[1]


        cur.execute("SELECT * FROM options WHERE id_q = %s" % id_q)
        options = cur.fetchall()


        return render_template('game.html', id_q=id_q, type=type, text=text, options=options)



@app.route('/form_action', methods=['GET', 'POST'])
def form_action():
    if request.method == 'POST':

        db = sqlite3.connect("mydatabase.db")
        cur = db.cursor()

        user_id = session['name']
        q_id = request.form['q_id']
        answer = request.form['answer']

        cur.execute("INSERT INTO answers (user, id_q, answer) VALUES (?, ?, ?)", (user_id, q_id, answer))
        db.commit()
        db.close()

        session['page'] = str((int(q_id)+1))

        return redirect('/game', code=302)
    else:
        return 'что-то не так'





def do_the_login(name, lastname):
    session['lastname'] = lastname
    session['name'] = name
    session['page'] = 1

    return redirect('/game', code=302)

def show_the_login_form(username):
    return render_template('login.html', username=username)



@app.route('/exit', methods=['GET', 'POST'])
def exit():
    session.clear()
    return redirect('/', code=302)




@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return do_the_login(request.form['name'], request.form['lastname'])
    else:
        return show_the_login_form('4')



app.secret_key = 'A0Zr98j/3gjojgfoitr9854984598]LWX/,?RT'


# conn.close()


if __name__ == '__main__':
    app.run(debug=True)
